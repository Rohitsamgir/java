package rohit;

public class oops7 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		horse h1 = new horse();
		h1.walk();
		//animal a1 = new animal;   cannot initiate the animal because it is abstract class
		h1.eats();  //calling the non abstract method of abstract class
		//abstract class can be non abstract
	}

}

abstract class animal{
	abstract void walk();
	
	public void eats () {
		System.out.println("Animal eats");
	}
	
	animal (){
		System.out.println("calling the animal constructer");
	}
}

class horse extends animal{
	public void walk() {
		System.out.println("walks on 4 legs");
	}
	
	horse (){
		System.out.println("calling the horse constructer");
	}
}

class chicken extends animal{
	public void walk() {
		System.out.println("walks on 2 legs");
	}
}
