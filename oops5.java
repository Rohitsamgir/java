package rohit;

public class oops5 {

	public static void main(String[] args) {
		triangle t1 = new triangle();
		t1.color="red";				    // triangle has the properties of shape class by inheritence
		t1.area();						//property of shape class
		t1.area( 5, 8);					//property of triangle class
		equilateraltrianle T1 = new equilateraltrianle();
		T1.area(3, 5);
	}

}

class shape{
	String color;
	public void area() {
		System.out.println("area of shape");
	}
	
}

class triangle extends shape{
	public void area(int l, int h) {
		System.out.println(l*h*1/2);
	}
}

class equilateraltrianle{
	public void area(int l,int h) {
		System.out.println(l*h*1/2);
	}
}