package rohit;

public class oops3 {

	public static void main(String[] args) {
		student2 s1 = new student2();
		s1.name="ketan";
		s1.age=24;
		student2 s2 = new student2(s1);
		s2.print();
	}

}

class student2{
	String name;
	int age;
	
	student2(student2 s2){						//creating copy constructer
		this.name=s2.name;
		this.age=s2.age;
	}
	
	student2( ) {
		
	}
	public void print() {
		System.out.println(this.name);
		System.out.println(this.age);
	}
}