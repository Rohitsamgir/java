package rohit;

public class oops4 {

	public static void main(String[] args) {
		student4 s1 = new student4();
		s1.name="omkar";
		s1.age=22;
		s1.print(s1.name);
		s1.print(s1.age);
		s1.print(s1.name,s1.age);
	}

}

class student4{
	String name;
	int age;
	
	public void print(String name,int age) {
		System.out.println(this.name+" "+this.age);
	}
	
	public void print(String name) {
		System.out.println(this.name);
	}

	public void print(int age) {
		System.out.println(this.age);
	}
}