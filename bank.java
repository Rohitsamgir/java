package rohit;

public class bank {
	public static void main(String args[]) {
		account account2 = new account();
		account2.name="public acces specifier";
		account2.work="default access specifier can be accessed in same package";
		account2.email="rohit99@gmail.com   only accessed from the packages subclasse ";
		account2.setpass("abc");	
		//private access specifiers we cannot access outside the class to access this we create a getters and setters methods
		
		System.out.println(account2.getpass());
	}
}

class account{
	public String name;
	String work;
	protected String email;
	private String pass;
	public void print() {
		System.out.println(this.name);
	}
	
	//getters & setters
	public String getpass() {
		return this.pass;
	}
	
	public void setpass(String pass) {
		this.pass=pass;
	} 
}